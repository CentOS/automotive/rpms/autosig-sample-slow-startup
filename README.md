# autosig-sample-slow-startup

This is a simple service that slows down the `boot-complete.target` by
30 seconds. This can be useful to verify if a boot-time checking
watchdog works.
